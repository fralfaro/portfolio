# Software Developer Portfolio

<a href="https://faam.gitlab.io/portfolio/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/portfolio-link-brightgreen"></a>
[![pipeline status](https://gitlab.com/FAAM/portfolio/badges/master/pipeline.svg)](https://gitlab.com/FAAM/portfolio/-/commits/master)


## Description

Personal web page like a portfolio (using [Jupyter Book](https://jupyterbook.org/intro.html)).




