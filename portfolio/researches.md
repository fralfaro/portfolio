# Researches & Talks

## Researches
- Alfaro,F. (2019). Estimation of the reciprocal effects between Happiness and Job Performance in Chile. Master thesis.
<a href="https://gitlab.com/FAAM/portfolio/-/blob/master/portfolio/files/researches/tesis_master.pdf" title="PDF" target="_new"> File: <img src="images/pdf.png"  width="15" height="15"></a>
  
- Alfaro,F. (2018/2019). University retention about construction engineering students. Investigation.
<a href="https://gitlab.com/FAAM" title="repository" target="_new"> Repository: <img src="images/gitlab.png"  width="15" height="15"></a>
  
- Alfaro,F. (2018). Spatial-statistics study for 3D Kriging (laboratory tests for tailings). Investigation.
<a href="https://gitlab.com/FAAM" title="repository" target="_new"> Repository: <img src="images/gitlab.png"  width="15" height="15"></a>

- Alfaro,F. (2017). Fraud detection in drinking water consumption. Degree thesis.
<a href="https://gitlab.com/FAAM/portfolio/-/blob/master/portfolio/files/researches/tesis_degree.pdf" title="PDF" target="_new"> File: <img src="images/pdf.png"  width="15" height="15"></a>
  

## Talks

- Alfaro,F. (2018). Estimation of the reciprocal effects between Happiness and Job Performance in Chile. **FNE33/CLATSE13 Congress**. 
Guadalajara, MX, October 04, 2018.
<a href="https://gitlab.com/FAAM/portfolio/-/blob/master/portfolio/files/talks/FNE33_talk.pdf" title="PDF" target="_new"> Talk: <img src="images/speech.png"  width="15" height="15"></a>
<a href="https://gitlab.com/FAAM/portfolio/-/blob/master/portfolio/files/talks/FNE33_val.pdf" title="PDF" target="_new"> Validation: <img src="images/page.jpeg"  width="15" height="15"></a>
